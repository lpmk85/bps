import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.*;
import java.security.spec.X509EncodedKeySpec;
import java.util.Random;

/**
 * A TCP server that runs on port 9090.  When a client connects, it
 * sends the client the current date and time, then closes the
 * connection with that client.  Arguably just about the simplest
 * server you can write.
 */
public class Server {
    private static BigInteger bobSharedSecret;
    private static BigInteger bobPrivateKey;
    private static BigInteger bobPublicKey;
    private static KeyPair rsaKeyPair;
    private static String keySignature;

    private static boolean breakSignature=false;

    public static void main(String[] args) throws Exception {
        ServerSocket listener = new ServerSocket(9090);

        bobPrivateKey = DiffieHellman.generatePrivateKey();
        System.out.println("Bob private Diffie-Hellman key: " + bobPrivateKey);

        bobPublicKey = DiffieHellman.generatePublicKey(bobPrivateKey);
        System.out.println("Bob public Diffie-Hellman key: " + bobPublicKey);

        rsaKeyPair=RSA.generateKeyPair();

        Socket socket = listener.accept();
        ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
        ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());

        //converts received bytes to RSA key
        byte[] encodedRsaPublicKey =(byte[]) in.readObject();
        X509EncodedKeySpec ks = new X509EncodedKeySpec(encodedRsaPublicKey);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PublicKey rsaPublicKey = kf.generatePublic(ks);
        System.out.println("RSA public key retrieved from Alice: " + rsaPublicKey);

        //receive RSA signature from Alice
        keySignature  = (String) in.readObject();
        System.out.println(System.lineSeparator() + "RSA sign for Alice public key: " + keySignature);

        //receive Diffie-Hellman public key from Alice
        BigInteger alicePublicKey  = (BigInteger) in.readObject();
        System.out.println("Alice Diffie-Hellman public key: "+alicePublicKey);

        //breaks signature if true (verification will fail)
        if(breakSignature)
            alicePublicKey=BigInteger.probablePrime(16, new Random());

        //check if signature is valid or not
        if(RSA.verify(alicePublicKey.toString(), keySignature, rsaPublicKey))
            System.out.println("Signature verified.");
        else {
            System.out.println("Invalid signature");
            return;
        }

        //send Diffie-Hellman public key to Alice
        out.writeObject(bobPublicKey);

        bobSharedSecret=DiffieHellman.generateSharedKey(alicePublicKey, bobPrivateKey);
        System.out.println("Bob Diffie-Hellman shared key: "+bobSharedSecret);

        //receive encrypted message from Alice
        String encMessage=(String) in.readObject();
        System.out.println("Encrypted message: "+encMessage);
        AESDecrypt aesDecrypt=new AESDecrypt(bobSharedSecret.toString());
        String decMessage=aesDecrypt.decrypt(encMessage);
        System.out.println("Received message after decryption: "+decMessage);


        socket.close();
    }
}