import java.io.*;
import java.math.BigInteger;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.JOptionPane;

public class Client {

    public static void main(String[] args) throws Exception {
        String serverAddress = JOptionPane.showInputDialog(
                "Enter IP Address of a machine that is\n" +
                        "running the date service on port 9090:");
        Socket socket = new Socket(serverAddress, 9090);

        KeyPair rsaKeyPair=RSA.generateKeyPair();
        System.out.println("Alice public RSA key: " + rsaKeyPair.getPublic());

        BigInteger alicePrivateKey = DiffieHellman.generatePrivateKey();
        System.out.println("Alice Diffie-Hellman private key: " + alicePrivateKey);

        BigInteger alicePublicKey = DiffieHellman.generatePublicKey(alicePrivateKey);
        System.out.println("Alice Diffie-Hellman public key: " + alicePublicKey);

        ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
        ObjectInputStream in = new ObjectInputStream(socket.getInputStream());

        //send public RSA key to Bob
        out.writeObject(rsaKeyPair.getPublic().getEncoded());

        //sign alice public key
        String signedKey=RSA.sign(alicePublicKey.toString(),rsaKeyPair.getPrivate());

        //send signed key and message (m,s)
        out.writeObject(signedKey);
        out.writeObject(alicePublicKey);

        //receive public Diffie-Hellman key from Bob
        BigInteger bobPublicKey = (BigInteger) in.readObject();
        System.out.println("Public Diffie-Hellman key received from Bob: " + bobPublicKey);

        BigInteger aliceSharedSecret = DiffieHellman.generateSharedKey(bobPublicKey, alicePrivateKey);
        System.out.println("Alice Diffie-Hellman shared key:  "+aliceSharedSecret);

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print(System.lineSeparator() + "Enter message:");
        String message=reader.readLine();

        //send message to Bob
        AESEncrypt aesEncrypt = new AESEncrypt(aliceSharedSecret.toString());
        String encData = aesEncrypt.encrypt(message);
        System.out.println("Encrypted message: "+encData);
        out.writeObject(encData);

        socket.close();

        System.exit(0);
    }
}
