import java.math.BigInteger;
import java.util.Random;

public class DiffieHellman {

    /** The large prime used for modular arithmetic. */
    private final static BigInteger P=new BigInteger("0123456789", 16);

    /** The generator used for modular arithmetic. */
    private final static BigInteger G = new BigInteger("2");

    /**
     * Returns a random 1568-bit integer that represents a private Diffie-Hellman key.
     * @return  a random large integer
     */
    public static BigInteger generatePrivateKey() {
        final byte[] largeNumber = new byte[196];
        final Random random = new Random();
        random.nextBytes(largeNumber);
        return new BigInteger(largeNumber);
    }

    /**
     * Returns a public key generated from a given private key.
     * @param privateKey    the private key from which to generate the public key
     * @return  a public key generated from a given private key
     */
    public static BigInteger generatePublicKey(BigInteger privateKey) {
        return G.modPow(privateKey, P);
    }

    /**
     * Returns a shared key generated from another party's public key and a local private key.
     * @param publicKey     the third-party public key
     * @param privateKey    the local private key
     * @return  a shared key generated from the public and private keys
     */
    public static BigInteger generateSharedKey(BigInteger publicKey, BigInteger privateKey) {
        return publicKey.modPow(privateKey, P);
    }
}